# myTask

# task list


1. Question:
Consider an array of integers. Write an algorithm to find the maximum element in the array.

2. Question:
Given an array of integers, write a function to find the sum of all the elements.

3. Question:
Write a function that takes an array of integers and returns the index of the first occurrence of a specific integer.

4. Question:
Implement an algorithm to reverse an array of integers in-place.

5. Question:
Write a function to remove duplicates from a sorted array of integers in-place and return the new length.