package task5;

import java.util.LinkedHashSet;

public class RemoveArray {
    public LinkedHashSet<Integer> removeDuplicates(int[] array) {
        LinkedHashSet<Integer> set = new LinkedHashSet<Integer>();
        for (int i = 0; i < array.length; i++) {
            set.add(array[i]);
        }
        return set;
    }
}
