package task5;

import task4.ReverseArray;
import util.CreateArray;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        CreateArray create = new CreateArray();
        RemoveArray remove = new RemoveArray();

        int[] array = create.createArray();
        System.out.println("Your array is: " + Arrays.toString(array));
        System.out.println("New array is: " + remove.removeDuplicates(array));
    }
}
