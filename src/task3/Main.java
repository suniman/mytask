package task3;

import util.CreateArray;

public class Main {
    public static void main(String[] args) {
        CreateArray create = new CreateArray();
        OccurrenceArray occurrence = new OccurrenceArray();

        int[] array = create.createArray();
        int target = create.target();


        System.out.println("First occurrence of " + target + " is " +
                "at index " + occurrence.findOccurrenceArray(array, target));

    }
}
