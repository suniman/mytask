package task3;

public class OccurrenceArray {
    public int findOccurrenceArray(int[] array, int target){
        for(int i = 0; i < array.length; i++){
            if (array[i] == target){
                return i;
            }
        } return -1;
    }
}
