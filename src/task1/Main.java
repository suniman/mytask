package task1;

import util.CreateArray;

public class Main {

    public static void main(String[] args) {
        FindArray find = new FindArray();
        CreateArray create = new CreateArray();

        int[] array = create.createArray();
        System.out.println("The max element is: " + find.findMaxElement(array));
    }
}
