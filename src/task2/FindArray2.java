package task2;

public class FindArray2 {

    public int findSumOfArray(int[] array) {
        int sum = 0;
        for (int number : array) {
            sum += number;
        }
        return sum;
    }
}
