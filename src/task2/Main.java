package task2;

import util.CreateArray;

public class Main {
    public static void main(String[] args) {
        FindArray2 find = new FindArray2();
        CreateArray create = new CreateArray();

        int[] array = create.createArray();
        System.out.print("The sum of all element is: " + find.findSumOfArray(array));
    }
}
