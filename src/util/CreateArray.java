package util;

import java.util.Scanner;

public class CreateArray {

    Scanner stdin = new Scanner(System.in);

    public int[] createArray() {


        System.out.print("Enter the array size: ");
        int count = stdin.nextInt();
        int[] array = new int[count];

        for (int i = 0; i < count; i++) {
            System.out.print("Enter the " + i + " element of array: ");
            array[i] = stdin.nextInt();
        }
        return array;
    }

    public int target() {
        System.out.print("Enter the target: ");
        int target = stdin.nextInt();
        return target;
    }
}
