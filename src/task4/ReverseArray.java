package task4;

public class ReverseArray {
    public int[] reverseArray(int[] array) {
        int[] reversedArray = new int[array.length];
        for (int i = 0; i < array.length; i++) {
            reversedArray[i] = array[array.length - i - 1];
        }
        return reversedArray;
    }

    public void reverseArrayVoid(int[] array) {
        int[] reversedArray = new int[array.length];
        for (int i = 0; i < array.length; i++) {
            reversedArray[i] = array[array.length - i - 1];
        }
        System.out.print("Reversed array is: [");
        for (int i : reversedArray) {
            System.out.print(i + ",");
        }
        System.out.println("]");

    }
}
