package task4;

import util.CreateArray;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        CreateArray create = new CreateArray();
        ReverseArray reverse = new ReverseArray();

        int[] array = create.createArray();
        System.out.println("Your array is: " + Arrays.toString(array));
        System.out.println("Reversed array is: " + Arrays.toString(reverse.reverseArray(array)));

//        reverse.reverseArrayVoid(array);
    }
}
